FROM centos:latest

RUN yum update -y && yum install python3 -y -y python3-pip -y
RUN pip3 install flask flask-jsonpify flask-restful
ADD python-api.py /python-api/
ENTRYPOINT [ "python3", "/python-api/python-api.py"]
